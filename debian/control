Source: kalk
Section: utils
Priority: optional
Maintainer: Han Young <hanyoung@protonmail.com>
Build-Depends: bison,
               cmake,
               debhelper-compat (= 13),
               flex,
               kf6-extra-cmake-modules,
               kf6-kconfig-dev,
               kf6-kcoreaddons-dev,
               kf6-ki18n-dev,
               kf6-kirigami2-dev,
               kf6-kunitconversion-dev,
               libmpfr-dev,
               libqalculate-dev,
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-svg-dev
Standards-Version: 4.6.2
Vcs-Git: https://invent.kde.org/neon/mobile/kalk
Vcs-Browser: https://invent.kde.org/neon/mobile/kalk

Package: kalk
Architecture: any
Depends: qml6-module-org-kde-kirigami2,
         qml6-module-qtquick-controls,
         ${misc:Depends},
         ${shlibs:Depends}
Description: A Kirigami based convergent calculator.
 Kalk is a convergent calculator application built with the
 Kirigami framework. Although it is mainly targeted for mobile
 platforms, it can also be used on the desktop.
 .
 Originally starting as a fork of Liri calculator, Kalk has gone
 through heavy development, and no longer shares the same codebase
 with Liri calculator.
